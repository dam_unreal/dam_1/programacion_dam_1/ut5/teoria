package ClasesInternas;

public class Alumno {
	//Variables
	private String nombre,apellidos,DNI,ciclo;
	private int edad,curso;
	Direccion d;
	
	//Constructor
	public Alumno(String nombre, String apellidos, String dNI, String ciclo, int edad, int curso) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		DNI = dNI;
		this.ciclo = ciclo;
		this.edad = edad;
		this.curso = curso;
		d = new Direccion("Hermanos Machado");
	}

	//Metodos GET
	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}
	
	public String getDNI() {
		return DNI;
	}
	
	public String getCiclo() {
		return ciclo;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public int getCurso() {
		return curso;
	}
	
	//Metodos SET
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}	

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}	

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setCurso(int curso) {
		this.curso = curso;
	}

	//Metodo toString
	public String toString() {
		return "Alumno [Nombre=" + nombre + ", Apellidos=" + apellidos + ", DNI=" + DNI + ", Ciclo=" + ciclo + ", Edad="
				+ edad + ", Curso=" + curso + ", Direccion=" + d + "]";
	}

	//Clase interna
	class Direccion{
		//Variables
		String calle;
		
		//Constructor
		Direccion(String calle){
			this.calle = calle;
		}

		//Metodos GET
		public String getCalle() {
			return calle;
		}

		//Metodos SET
		public void setCalle(String calle) {
			this.calle = calle;
		}

		//Metodo toString
		public String toString() {
			return "Calle "+calle;
		}		
	}
	
	//MAIN
	public static void main(String[] args) {
		Alumno a = new Alumno("Jose Andres", "Sanchez Perez", "1234V", "DAM", 20, 1);
		System.out.println(a.toString());
	}
}