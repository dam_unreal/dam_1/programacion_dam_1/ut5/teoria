package ClasesInternas;

public class Prueba {
	int a = 10;
	
	public Prueba(){
		PruebaB b = new PruebaB();
		Prueba.PruebaB ab = new Prueba.PruebaB();		
		b.metodo();
		ab.metodo();
	}
	
	class PruebaB{
		void metodo(){
			System.out.println(a);
		}
	}

	public static void main(String[] args) {
		new Prueba();
	}
}