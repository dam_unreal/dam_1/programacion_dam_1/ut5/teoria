package ClasesInternas;

public class Lampara {
	private String forma;
	private boolean encendido;
	private Bombilla bombilla;
	
	public Lampara(String forma){
		this.forma = forma;
		encendido = false;
		bombilla = new Bombilla(0);
	}
	
	public String getForma(){
		return forma;
	}
	
	public void setForma(String forma){
		this.forma = forma;
	}
	
	public void cambiarBombilla(int potencia){
		bombilla = new Bombilla(potencia);
	}
	
	public void encenderLampara(){
		encendido = true;
	}
	
	public void apagarLampara(){
		encendido = false;
	}
	
	public String toString() {
		return "Lampara [forma=" + forma + ", encendido=" + encendido + ", bombilla=" + bombilla + "]";
	}

	public class Bombilla{
		int potencia;
		
		public Bombilla(int p) {
			potencia = p;
		}
		
		public void setPotencia(int potencia){
			this.potencia = potencia;
		}
		
		public int getPotencia(){
			return potencia;
		}

		public String toString() {
			return "Bombilla [potencia=" + potencia + "]";
		}	
	}
	
	public static void main(String[] args) {
		Lampara l = new Lampara("a");;
		System.out.println(l.toString());
		System.out.println(l.bombilla.toString());
		l.encenderLampara();
		l.cambiarBombilla(5);
		System.out.println(l.toString());
		System.out.println(l.bombilla.toString());
	}
}