package ClasesInternas;

public class ClaseInternaLocal {
	private int a = 10;
	private static int b = 5;
	
	public void metodo(){ //METODO NORMAL Y CORRIENTE
		final int c = 0;
		
		class B{ //CLASE INTERNA LOCAL (DENTRO DE UN METODO)(SOLO SE PUEDE TRABAJAR CON ELLA DENTRO DEL METODO)
			void otroMetodo(){
				System.out.println(a);
				System.out.println(b);
				System.out.println(c);
			}
		}
		
		B in = new B();
		in.otroMetodo();
	}
	
	public static void main(String []args){
		new ClaseInternaLocal().metodo();
	}	
}