package ClasesInternas;

public class Externa {
	private int datoExterno = 5;
	
	class Interna{
		private int datoInterno = 10;
		
		void metodo(){
			System.out.println("Desde la clase externa: "+datoExterno);
			System.out.println("Desde la clase interno: "+datoInterno);
		}
	}
	
	public static void main(String[] args) {
		Externa ce = new Externa();
		Interna ci = ce.new Interna();
		System.out.println(ce.datoExterno);
		System.out.println(ci.datoInterno);
		ci.metodo();
	}

}