package ClasesInternas;

public class ClaseInternaStatic {
	private int a = 10;
	private static int b = 5;	
			
	static class B{ //CLASE INTERNA ESTATICA
		B(){
			ClaseInternaStatic S = new ClaseInternaStatic();
			System.out.println(S.a);
			System.out.println(b);
		}
	}
	
	public static void main(String []args){
		ClaseInternaStatic.B ab = new ClaseInternaStatic.B();
	}	
}