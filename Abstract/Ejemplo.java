package Abstract;

public class Ejemplo {
	public static void main(String[] args){}
}

abstract class Figura{
	
	abstract double area();
	abstract double perimetro();
	
}

class Circulo extends Figura{
	
	private double radio;
	
	public Circulo(double radio) {
		super();
		this.radio = radio;
	}
	@Override
	public String toString() {
		return "Circulo\n"
				+ "Area: "+area()+"\n"
				+ "Perimetro: "+perimetro();
	}
	public double getRadio() {
		return radio;
	}
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	double area(){
		return Math.PI*Math.pow(radio, 2);
	}
	double perimetro(){
		return 2*Math.PI*radio;
	}
}

class Rectangulo extends Figura{
	
	private double base,altura;
	
	
	public Rectangulo(double base, double altura) {
		super();
		this.base = base;
		this.altura = altura;
	}
	@Override
	public String toString() {
		return "Rectangulo\n"
				+ "Area: "+area()+"\n"
				+ "Perimetro: "+perimetro();
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	double area(){
		return base*altura;
	}
	double perimetro(){
		return base*2+altura*2;
	}
}

class Triangulo extends Figura{
	
	private double base,altura,lado1,lado2,lado3;
	
	public Triangulo(double base, double altura, double lado1, double lado2, double lado3) {
		super();
		this.base = base;
		this.altura = altura;
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;		
	}
	@Override
	public String toString() {
		return "Trangulo\n"
				+ "Area: "+area()+"\n"
				+ "Perimetro: "+perimetro();
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getLado1() {
		return lado1;
	}
	public void setLado1(double lado1) {
		this.lado1 = lado1;
	}
	public double getLado2() {
		return lado2;
	}
	public void setLado2(double lado2) {
		this.lado2 = lado2;
	}
	public double getLado3() {
		return lado3;
	}
	public void setLado3(double lado3) {
		this.lado3 = lado3;
	}
	
	double area(){
		return (base*altura)/2;
	}
	double perimetro(){
		return lado1+lado2+lado2;
	}
}