package Interfaces;

public class MiNuevaInterface implements MiInterfaz {

	@Override
	public int sumar(int arg1, int arg2) {
		return arg1+arg2;
	}

	@Override
	public int restar(int arg1, int arg2) {
		return arg1-arg2;
	}

}
