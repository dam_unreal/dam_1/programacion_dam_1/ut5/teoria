package Interfaces;

import java.util.Scanner;

public class Figuras{
	
	 public static void main(String[] args){
		 
		 Scanner sc = new Scanner(System.in);
		 int opcion=0,base = 0, altura = 0;
		 double radio = 0;
		 
		 do{
			 System.out.println("1 -> Circulo\n2 -> Rectangulo\n3 -> Trianuglo");
			 try{
				 opcion = sc.nextInt();
			 }catch(Exception e){
				 opcion = 0;
			 }finally{
				 sc.nextLine();
			 }	 
		 }while(opcion!=1 && opcion!=2 && opcion!=3);
		 
		 switch(opcion){
		 	case 1:
		 		do{
					 System.out.println("¿Radio circulo?");
					 try{
						 radio = sc.nextDouble();
					 }catch(Exception e){
						 radio = 0;
					 }finally{
						 sc.nextLine();
					 }	 
				 }while(radio<=0);
		 		
		 		Circulo c = new Circulo(radio);
		 		System.out.println(c.toString());	
		 		break;
		 	case 2:
		 		do{
					 System.out.println("Escribe la base y la altura");
					 try{
						 base = sc.nextInt();
						 altura = sc.nextInt();
					 }catch(Exception e){
						 base = 0;
						 altura = 0;
					 }finally{
						 sc.nextLine();
					 }	 
				 }while(base<=0 && altura<=0);
		 		
		 		Rectangulo r = new Rectangulo(base,altura);
		 		System.out.println(r.toString());
		 		break;
		 	case 3:
		 		do{
					 System.out.println("Escribe la base y la altura");
					 try{
						 base = sc.nextInt();
						 altura = sc.nextInt();
					 }catch(Exception e){
						 base = 0;
						 altura = 0;
					 }finally{
						 sc.nextLine();
					 }	 
				 }while(base<=0 && altura<=0);
		 		
		 		Triangulo t = new Triangulo(base,altura);
		 		System.out.println(t.toString());
		 		break;
		 }		 
	 }
}

class Circulo implements InterfazFiguras{
	private double radio;
	
	public Circulo(double radio) {
		super();
		this.radio = radio;
	}
	@Override
	public String toString() {
		return "Circulo\n"
				+ "Area: "+area()+"\n"
				+ "Perimetro: "+perimetro();
	}
	public double getRadio() {
		return radio;
	}
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
 	public double area(){
		return Math.PI*Math.pow(radio, 2);
	}
	public double perimetro(){
		return 2*Math.PI*radio;
	}
}

class Rectangulo implements InterfazFiguras{	
	private double base,altura;
	
	public Rectangulo(double base, double altura) {
		super();
		this.base = base;
		this.altura = altura;
	}
	public String toString() {
		return "Rectangulo\n"
				+ "Area: "+area()+"\n"
				+ "Perimetro: "+perimetro();
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public double area(){
		return base*altura;
	}
	public double perimetro(){
		return base*2+altura*2;
	}
}

class Triangulo implements InterfazFiguras{
	private double base,altura;
	
	public Triangulo(double base, double altura) {
		super();
		this.base = base;
		this.altura = altura;
	}
	
	@Override
	public String toString() {
		return "Trangulo\n"
				+ "Area: "+area();
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	
	public double area(){
		return (base*altura)/2;
	}
	public double perimetro() {
		return 0;
	}
}