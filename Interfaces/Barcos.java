package Interfaces;

public class Barcos {
	
	private String nombre;

	public Barcos(){}

	public Barcos(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "Nombre: "+nombre+"\n";
	}
	
	public String msgSocorro(){
		return "SOS desde el "+nombre;
	}
	
	public String alarma(){
		return "Alarma desde el "+nombre;
	}
}

class BarcoPasajeros extends Barcos{
	
	private double eslora;
	private int numeroCamas;
	
	public BarcoPasajeros(){
		super();
	}
	
	public BarcoPasajeros(String nombre,double eslora, int numeroCarnas) {
		super(nombre);
		this.eslora = eslora;
		this.numeroCamas = numeroCarnas;
	}
	
	public double getEslora() {
		return eslora;
	}
	
	public void setEslora(double eslora) {
		this.eslora = eslora;
	}
	
	public int getNumeroCarnas() {
		return numeroCamas;
	}
	
	public void setNumeroCarnas(int numeroCarnas) {
		this.numeroCamas = numeroCarnas;
	}
	
	@Override
	public String toString() {
		return super.toString()+"Eslora: " + eslora + ", Numero camas: " + numeroCamas;
	}
	
	public String msgSocorro(String msg){
		return super.msgSocorro()+", "+msg;
	}
	
	public String alarma(){
		return super.alarma()+" barco de pasajeros con "+numeroCamas+" y "+eslora+" esloras";
	}
	
}

class Pesquero extends Barcos{
	
	private double eslora;
	private int potencia, tripulacion;
	
	public Pesquero() {
		super();
	}

	public Pesquero(String nombre, double eslora, int potencia, int tripulacion) {
		super(nombre);
		this.eslora = eslora;
		this.potencia = potencia;
		this.tripulacion = tripulacion;
	}

	public double getEslora() {
		return eslora;
	}

	public void setEslora(double eslora) {
		this.eslora = eslora;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public int getTripulacion() {
		return tripulacion;
	}

	public void setTripulacion(int tripulacion) {
		this.tripulacion = tripulacion;
	}

	@Override
	public String toString() {
		return super.toString()+" Eslora: " + eslora + ", Potencia: " + potencia + ", Tripulacion: " + tripulacion;
	}

	public String msgSocorro(String msg){
		return super.msgSocorro()+", "+msg;
	}
	
	public String alarma(){
		return super.alarma()+" con "+tripulacion+" de tripulacion";
	}
	
}

	class PortaAviones extends Barcos{
		
		private int numeroAviones, tripulantes;

		public PortaAviones() {
			super();
			// TODO Apéndice de constructor generado automáticamente
		}

		public PortaAviones(String nombre, int numeroAviones, int tripulantes) {
			super(nombre);
			this.numeroAviones = numeroAviones;
			this.tripulantes = tripulantes;
		}

		public int getNumeroAviones() {
			return numeroAviones;
		}

		public void setNumeroAviones(int numeroAviones) {
			this.numeroAviones = numeroAviones;
		}

		public int getTripulantes() {
			return tripulantes;
		}

		public void setTripulantes(int tripulantes) {
			this.tripulantes = tripulantes;
		}

		@Override
		public String toString() {
			return super.toString()+" Numero aviones: " + numeroAviones + ", Tripulantes=" + tripulantes;
		}
		
		public String msgSocorro(String msg){
			return super.msgSocorro()+", "+msg;
		}
		
		public String alarma(){
			return super.alarma()+" con "+tripulantes+" de tripulacion";
		}
		
	}