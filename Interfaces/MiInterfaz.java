package Interfaces;

public interface MiInterfaz {

	int MAXIMO = 10;
	int MINIMO = 1;
	
	int sumar(int arg1, int arg2);
	int restar(int arg1, int arg2);
}
