package Interfaces;

public interface InterfazFiguras {
		
	double area();
	double perimetro();
		
}