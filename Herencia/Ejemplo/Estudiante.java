package Herencia.Ejemplo;

public class Estudiante extends Persona{

	private int curso;
	private Grado grado;

	public Estudiante(){
		super();
		System.out.println("Generado en Estudiante");
	}
	
	public Estudiante(int curso) {
		super();
		this.curso = curso;
	}
	
	public Estudiante(String nombre, String apellido, String direccion ,int edad,Grado grado, int curso){
		super(nombre, apellido, direccion, edad); //Esto debe estar primero SIEMPRE si la clase hereda de otra
		System.out.println("Generado en Estudiante");
		this.curso = curso;
		this.grado = grado;
	}

	public int getCurso() {
		return curso;
	}

	public void setCurso(int curso) {
		this.curso = curso;
	}

	@Override
	public String toString() {
		return super.toString()+"\nCurso=" + curso +"\nGrado="+grado;
	}
	
}