package Herencia.Ejemplo;

public class Persona {

	private String nombre, apellidos, direccion;
	private int edad;
	
	public Persona() {
		System.out.println("Generado en Persona");
	}
	
	public Persona(String nombre, String apellidos, String direccion, int edad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String dirreccion) {
		this.direccion = dirreccion;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Nombre=" + nombre + "\nApellidos=" + apellidos + "\nDireccion=" + direccion + "\nEdad="
				+ edad;
	}
	
}
