package Polimorfismo;

public class BarcosPrincial {

	public static void main(String[] args) {
		
		BarcoPasajeros Titanic = new BarcoPasajeros("Titanic",269,1000);
		Pesquero p = new Pesquero("Estabilidad",10,20,30);
		PortaAviones Skere = new PortaAviones("SKERE", 10, 100);
		
		System.out.println(Titanic.toString());
		System.out.println(Titanic.alarma());
		System.out.println(Titanic.msgSocorro("Socorro viejo lesbiano, me hundo"));
		
		System.out.println();
		
		System.out.println(p.toString());
		System.out.println(p.alarma());
		System.out.println(p.msgSocorro("lloro"));

		System.out.println();
		
		System.out.println(Skere.toString());
		System.out.println(Skere.alarma());
		System.out.println(Skere.msgSocorro("lloro"));
	}

}
